import { AddUserType, Controller } from './add-user-type-controller-protocols'
import { badRequest, noContent, serverError } from '@/presentation/helpers/http/http-helper'
import { HttpRequest, HttpResponse, Validation } from '@/presentation/protocols'

export class AddUserTypeController implements Controller {
  constructor (private readonly validation: Validation, private readonly addUserType: AddUserType) {}

  async handle (httpRequest: HttpRequest): Promise<HttpResponse> {
    try {
      const error = await this.validation.validate(httpRequest.body)

      if (error) {
        return badRequest(error)
      }

      const { name, role } = httpRequest.body

      await this.addUserType.add({
        name,
        role
      })

      return noContent()
    } catch (error) {
      return serverError(error)
    }
  }
}
