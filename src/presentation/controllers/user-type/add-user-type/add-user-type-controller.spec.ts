import { HttpRequest, Validation, AddUserType, AddUserTypeModel } from './add-user-type-controller-protocols'
import { AddUserTypeController } from './add-user-type-controller'
import { badRequest, noContent, serverError } from '@/presentation/helpers/http/http-helper'

const makeFakeRequest = (): HttpRequest => ({
  body: {
    name: 'any_name',
    role: 'any_role'
  }
})

const makeValidation = (): Validation => {
  class ValidationStub implements Validation {
    validate (input: any): Error | null {
      return null
    }
  }

  return new ValidationStub()
}

const makeAddUserType = (): AddUserType => {
  class AddUserTypeStub implements AddUserType {
    async add (data: AddUserTypeModel): Promise<void> {
      return await new Promise(resolve => resolve())
    }
  }

  return new AddUserTypeStub()
}

type SutTypes = {
  sut: AddUserTypeController
  validationStub: Validation
  addUserTypeStub: AddUserType
}

const makeSut = (): SutTypes => {
  const validationStub = makeValidation()
  const addUserTypeStub = makeAddUserType()
  const sut = new AddUserTypeController(validationStub, addUserTypeStub)

  return { sut, validationStub, addUserTypeStub }
}

describe('Add User Type', () => {
  test('Should call Validation with correct values', async () => {
    const { sut, validationStub } = makeSut()

    const validateSpy = jest.spyOn(validationStub, 'validate')

    const httpRequest = makeFakeRequest()

    await sut.handle(httpRequest)

    expect(validateSpy).toHaveBeenCalledWith(httpRequest.body)
  })

  test('Should return 400 if Validation fails', async () => {
    const { sut, validationStub } = makeSut()

    jest.spyOn(validationStub, 'validate').mockReturnValueOnce(new Error())

    const httpResponse = await sut.handle(makeFakeRequest())

    expect(httpResponse).toEqual(badRequest(new Error()))
  })

  test('Should call AddUserType with correct values', async () => {
    const { sut, addUserTypeStub } = makeSut()

    const addSpy = jest.spyOn(addUserTypeStub, 'add')

    const httpRequest = makeFakeRequest()

    await sut.handle(httpRequest)

    expect(addSpy).toHaveBeenCalledWith(httpRequest.body)
  })

  test('Should return 500 if AddUSerType throws', async () => {
    const { sut, addUserTypeStub } = makeSut()

    jest.spyOn(addUserTypeStub, 'add').mockReturnValueOnce(new Promise((_resolve, reject) => reject(new Error())))

    const httpResponse = await sut.handle(makeFakeRequest())

    expect(httpResponse).toEqual(serverError(new Error()))
  })

  test('Should return 204 on success', async () => {
    const { sut } = makeSut()

    const httpResponse = await sut.handle(makeFakeRequest())

    expect(httpResponse).toEqual(noContent())
  })
})
