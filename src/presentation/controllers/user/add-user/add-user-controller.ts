import { Controller, HttpRequest, HttpResponse, AddUser, Validation, Randomize } from './add-user-controller-protocols'
import { badRequest, forbidden, serverError, success } from '@/presentation/helpers/http/http-helper'
import { EmailInUseError, NotFoundParamError } from '@/presentation/errors'

export class AddUserController implements Controller {
  constructor (
    private readonly addUser: AddUser,
    private readonly validation: Validation,
    private readonly randomize: Randomize
  ) {}

  async handle (httpRequest: HttpRequest): Promise<HttpResponse> {
    try {
      const error = this.validation.validate(httpRequest.body)

      if (error) {
        return badRequest(error)
      }

      const { email, name, userTypeRole } = httpRequest.body

      const password = this.randomize.generate()
      console.log({ password })

      const user = await this.addUser.add({
        name,
        email,
        password,
        userTypeRole,
        firstAccess: true
      })

      if (!user.data && user.error) {
        if (user.error === 'EmailInUseError') {
          return forbidden(new EmailInUseError())
        }

        if (user.error === 'UserTypeNotFoundError') {
          return badRequest(new NotFoundParamError('userTypeRole'))
        }
      }

      return success({ user: user.data })
    } catch (error) {
      return serverError(error)
    }
  }
}
