import { LoadUsers, UserModel } from './load-users-controller-protocols'
import { LoadUsersController } from './load-users-controller'
import { noContent, serverError, success } from '@/presentation/helpers/http/http-helper'

const makeFakeUsers = (): UserModel[] => ([{
  id: 'valid_id',
  name: 'valid_name',
  email: 'valid_email@mail.com',
  password: 'hashed_password',
  userType: {
    id: 'valid_id',
    name: 'valid_name',
    role: 'valid_role'
  }
}])

type SutTypes = {
  sut: LoadUsersController
  loadUsersStub: LoadUsers
}

const makeLoadUsers = (): LoadUsers => {
  class LoadUsersStub implements LoadUsers {
    async load (): Promise<UserModel[]> {
      return await new Promise(resolve => resolve(makeFakeUsers()))
    }
  }

  return new LoadUsersStub()
}

const makeSut = (): SutTypes => {
  const loadUsersStub = makeLoadUsers()
  const sut = new LoadUsersController(loadUsersStub)

  return { sut, loadUsersStub }
}

describe('LoadUsers Controller', () => {
  test('Should call LoadUsers', async () => {
    const { sut, loadUsersStub } = makeSut()

    const loadSpy = jest.spyOn(loadUsersStub, 'load')

    await sut.handle({})
    expect(loadSpy).toHaveBeenCalled()
  })

  test('Should return 200 on success', async () => {
    const { sut } = makeSut()

    const httpResponse = await sut.handle({})
    expect(httpResponse).toEqual(success(makeFakeUsers()))
  })

  test('Should return 204 if loadUsers returns empty', async () => {
    const { sut, loadUsersStub } = makeSut()

    jest.spyOn(loadUsersStub, 'load').mockReturnValueOnce(new Promise((resolve) => resolve([])))

    const httpResponse = await sut.handle({})
    expect(httpResponse).toEqual(noContent())
  })

  test('Should return 500 if LoadUsers throws', async () => {
    const { sut, loadUsersStub } = makeSut()

    jest.spyOn(loadUsersStub, 'load').mockReturnValueOnce(new Promise((_resolve, reject) => reject(new Error())))

    const httpResponse = await sut.handle({})

    expect(httpResponse).toEqual(serverError(new Error()))
  })
})
