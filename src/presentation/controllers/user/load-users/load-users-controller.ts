import { Controller, HttpRequest, HttpResponse, LoadUsers } from './load-users-controller-protocols'
import { noContent, serverError, success } from '@/presentation/helpers/http/http-helper'

export class LoadUsersController implements Controller {
  constructor (private readonly loadUsers: LoadUsers) {}

  async handle (httpRequest: HttpRequest): Promise<HttpResponse> {
    try {
      const users = await this.loadUsers.load()

      return users.length ? success(users) : noContent()
    } catch (error) {
      return serverError(error)
    }
  }
}
