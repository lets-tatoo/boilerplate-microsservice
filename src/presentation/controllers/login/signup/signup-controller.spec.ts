import { SignUpController } from './signup-controller'
import { UserModel, AddUser, AddUserModel, HttpRequest, Validation, Authentication, AuthenticationModel, AddUSerResponseModel } from './signup-controller-protocols'
import { EmailInUseError, MissingParamError, NotFoundParamError, ServerError } from '@/presentation/errors'
import { badRequest, forbidden, serverError, success, unauthorized } from '@/presentation/helpers/http/http-helper'

type SutTypes = {
  sut: SignUpController
  addUserStub: AddUser
  validationStub: Validation
  authenticationStub: Authentication
}

const makeAddUser = (): AddUser => {
  class AddUserStub implements AddUser {
    async add (user: AddUserModel): Promise<AddUSerResponseModel> {
      return await new Promise(resolve => resolve({ data: makeFakeUser() }))
    }
  }

  return new AddUserStub()
}

const makeValidation = (): Validation => {
  class ValidationStub implements Validation {
    validate (input: any): Error | null {
      return null
    }
  }

  return new ValidationStub()
}

const makeAuthentication = (): Authentication => {
  class AuthenticationStub implements Authentication {
    async auth (authentication: AuthenticationModel): Promise<string> {
      return await new Promise(resolve => resolve('any_token'))
    }
  }

  return new AuthenticationStub()
}

const makeSut = (): SutTypes => {
  const addUserStub = makeAddUser()
  const validationStub = makeValidation()
  const authenticationStub = makeAuthentication()
  const sut = new SignUpController(addUserStub, validationStub, authenticationStub)

  return { sut, addUserStub, validationStub, authenticationStub }
}

const makeFakeUser = (): UserModel => ({
  id: 'valid_id',
  name: 'valid_name',
  email: 'valid_email@mail.com',
  password: 'valid_password',
  userType: {
    id: 'valid_id',
    name: 'valid_name',
    role: 'valid_role'
  }
})

const makeFakeRequest = (): HttpRequest => ({
  body: {
    name: 'any_name',
    email: 'any_email@mail.com',
    password: 'any_password',
    passwordConfirmation: 'any_password',
    userTypeRole: 'any_role'
  }
})

describe('SignUp Controller', () => {
  test('Should call AddUser with correct values', async () => {
    const { sut, addUserStub } = makeSut()

    const addSpy = jest.spyOn(addUserStub, 'add')

    await sut.handle(makeFakeRequest())
    expect(addSpy).toHaveBeenCalledWith({
      name: 'any_name',
      email: 'any_email@mail.com',
      password: 'any_password',
      userTypeRole: 'any_role'
    })
  })

  test('Should return 500 if AddUser throws', async () => {
    const { sut, addUserStub } = makeSut()

    jest.spyOn(addUserStub, 'add').mockImplementationOnce(async () => {
      return await new Promise((_resolve, reject) => reject(new Error()))
    })

    const httpResponse = await sut.handle(makeFakeRequest())
    expect(httpResponse).toEqual(serverError(new ServerError('')))
  })

  test('Should return 403 if AddUser returns EmailInUseError error', async () => {
    const { sut, addUserStub } = makeSut()

    jest.spyOn(addUserStub, 'add').mockReturnValueOnce(new Promise(resolve => resolve({ error: 'EmailInUseError' })))

    const httpResponse = await sut.handle(makeFakeRequest())
    expect(httpResponse).toEqual(forbidden(new EmailInUseError()))
  })

  test('Should return 401 if userTypeRole is an admin role', async () => {
    const { sut } = makeSut()

    const httpRequest = makeFakeRequest()
    httpRequest.body.userTypeRole = 'admin'

    const httpResponse = await sut.handle(httpRequest)
    expect(httpResponse).toEqual(unauthorized())
  })

  test('Should return 400 if AddUser returns UserTypeNotFoundError error', async () => {
    const { sut, addUserStub } = makeSut()

    jest.spyOn(addUserStub, 'add').mockReturnValueOnce(new Promise(resolve => resolve({ error: 'UserTypeNotFoundError' })))

    const httpResponse = await sut.handle(makeFakeRequest())
    expect(httpResponse).toEqual(badRequest(new NotFoundParamError('userTypeRole')))
  })

  test('Should return 200 if valid data is provided', async () => {
    const { sut } = makeSut()

    const httpResponse = await sut.handle(makeFakeRequest())
    expect(httpResponse).toEqual(success({ accessToken: 'any_token' }))
  })

  test('Should call Validation with correct values', async () => {
    const { sut, validationStub } = makeSut()

    const validateSpy = jest.spyOn(validationStub, 'validate')
    const httpRequest = makeFakeRequest()

    await sut.handle(httpRequest)
    expect(validateSpy).toHaveBeenCalledWith(httpRequest.body)
  })

  test('Should return 400 if Validation returns an error', async () => {
    const { sut, validationStub } = makeSut()

    jest.spyOn(validationStub, 'validate').mockReturnValueOnce(new MissingParamError('any_field'))

    const httpResponse = await sut.handle(makeFakeRequest())
    expect(httpResponse).toEqual(badRequest(new MissingParamError('any_field')))
  })

  test('Should call Authentication with correct values', async () => {
    const { sut, authenticationStub } = makeSut()

    const authSpy = jest.spyOn(authenticationStub, 'auth')

    await sut.handle(makeFakeRequest())

    expect(authSpy).toHaveBeenCalledWith({ email: 'any_email@mail.com', password: 'any_password' })
  })

  test('Should return 500 if Authentication throws', async () => {
    const { sut, authenticationStub } = makeSut()

    jest.spyOn(authenticationStub, 'auth').mockReturnValueOnce(new Promise((_resolve, reject) => reject(new Error())))

    const httpResponse = await sut.handle(makeFakeRequest())

    expect(httpResponse).toEqual(serverError(new Error()))
  })
})
