import { Controller, HttpRequest, HttpResponse, AddUser, Validation, Authentication } from './signup-controller-protocols'
import { badRequest, forbidden, serverError, success, unauthorized } from '@/presentation/helpers/http/http-helper'
import { EmailInUseError, NotFoundParamError } from '@/presentation/errors'

export class SignUpController implements Controller {
  constructor (
    private readonly addUser: AddUser,
    private readonly validation: Validation,
    private readonly authentication: Authentication
  ) {}

  async handle (httpRequest: HttpRequest): Promise<HttpResponse> {
    try {
      const error = this.validation.validate(httpRequest.body)

      if (error) {
        return badRequest(error)
      }

      const { password, email, name, userTypeRole } = httpRequest.body

      if (userTypeRole === 'admin') return unauthorized()

      const user = await this.addUser.add({
        name,
        email,
        password,
        userTypeRole
      })

      if (!user.data && user.error) {
        if (user.error === 'EmailInUseError') {
          return forbidden(new EmailInUseError())
        }

        if (user.error === 'UserTypeNotFoundError') {
          return badRequest(new NotFoundParamError('userTypeRole'))
        }
      }

      const accessToken = await this.authentication.auth({ email, password })

      return success({ accessToken })
    } catch (error) {
      return serverError(error)
    }
  }
}
