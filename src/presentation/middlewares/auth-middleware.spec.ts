import { AuthMiddleware } from './auth-middleware'
import { LoadUserByToken, HttpRequest, UserModel } from './auth-middleware-protocols'
import { forbidden, serverError, success } from '../helpers/http/http-helper'
import { AccessDeniedError } from '../errors'

const makeFakeUser = (): UserModel => ({
  id: 'valid_id',
  name: 'valid_name',
  email: 'valid_email@mail.com',
  password: 'hashed_password',
  userType: {
    id: 'valid_id',
    name: 'valid_name',
    role: 'valid_role'
  }
})

const makeFakeRequest = (): HttpRequest => ({
  headers: {
    'x-access-token': 'any_token'
  }
})

type SutTypes = {
  sut: AuthMiddleware
  loadUserByTokenStub: LoadUserByToken
}

const makeLoadUserByToken = (): LoadUserByToken => {
  class LoadUserByTokenStub implements LoadUserByToken {
    async load (accessToken: string, role?: string): Promise<UserModel> {
      return await new Promise(resolve => resolve(makeFakeUser()))
    }
  }

  return new LoadUserByTokenStub()
}

const makeSut = (role?: string): SutTypes => {
  const loadUserByTokenStub = makeLoadUserByToken()
  const sut = new AuthMiddleware(loadUserByTokenStub, role)

  return { sut, loadUserByTokenStub }
}

describe('Auth Middleware', () => {
  test('Should return 403 if no x-access-token is exists in headers', async () => {
    const { sut } = makeSut()

    const httpResponse = await sut.handle({})
    expect(httpResponse).toEqual(forbidden(new AccessDeniedError()))
  })

  test('Should call LoadUserByToken with correct accessToken', async () => {
    const role = 'any_role'
    const { sut, loadUserByTokenStub } = makeSut(role)

    const loadSpy = jest.spyOn(loadUserByTokenStub, 'load')

    await sut.handle(makeFakeRequest())
    expect(loadSpy).toHaveBeenCalledWith('any_token', role)
  })

  test('Should return 403 if LoadUserByToken returns null', async () => {
    const { sut, loadUserByTokenStub } = makeSut()

    jest.spyOn(loadUserByTokenStub, 'load').mockReturnValueOnce(new Promise(resolve => resolve(null)))

    const httpResponse = await sut.handle(makeFakeRequest())
    expect(httpResponse).toEqual(forbidden(new AccessDeniedError()))
  })

  test('Should return 200 if LoadUserByToken returns an user', async () => {
    const { sut } = makeSut()

    const httpResponse = await sut.handle(makeFakeRequest())
    expect(httpResponse).toEqual(success({ userId: 'valid_id' }))
  })

  test('Should return 500 if LoadUserByToken thorws', async () => {
    const { sut, loadUserByTokenStub } = makeSut()

    jest.spyOn(loadUserByTokenStub, 'load').mockReturnValueOnce(new Promise((_resolve, reject) => reject(new Error())))

    const httpResponse = await sut.handle(makeFakeRequest())
    expect(httpResponse).toEqual(serverError(new Error()))
  })
})
