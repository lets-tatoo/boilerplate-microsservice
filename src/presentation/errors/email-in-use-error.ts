export class EmailInUseError extends Error {
  constructor () {
    super('The receivd email is already in use')
    this.name = 'EmailInUseError'
  }
}
