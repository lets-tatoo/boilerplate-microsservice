export class NotFoundParamError extends Error {
  constructor (paramName: string) {
    super(`Not Found param: ${paramName}`)
    this.name = 'NotFoundParamError'
  }
}
