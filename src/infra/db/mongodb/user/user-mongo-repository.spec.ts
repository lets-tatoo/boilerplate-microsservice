import { UserMongoRepository } from './user-mongo-repository'
import { MongoHelper } from '../helpers/mongo-helper'
import { Collection } from 'mongodb'

describe('User Mongo Repository', () => {
  const url = process.env.MONGO_URL as string
  let userCollection: Collection
  let userTypeCollection: Collection

  beforeAll(async () => {
    await MongoHelper.connect(url)
  })

  afterAll(async () => {
    await MongoHelper.disconnect()
  })

  beforeEach(async () => {
    userCollection = await MongoHelper.getCollection('users')
    userTypeCollection = await MongoHelper.getCollection('user-types')

    await userTypeCollection.insertOne({
      name: 'Administrator',
      role: 'admin'
    })
  })

  afterEach(async () => {
    await userCollection.deleteMany({})
    await userTypeCollection.deleteMany({})
  })

  const makeSut = (): UserMongoRepository => {
    return new UserMongoRepository()
  }

  describe('add()', () => {
    test('Should return an user on add success', async () => {
      const sut = makeSut()

      await userTypeCollection.insertOne({
        name: 'any_name',
        role: 'any_role'
      })

      const user = await sut.add({
        name: 'any_name',
        email: 'any_email@mail.com',
        password: 'any_password',
        userTypeRole: 'any_role'
      })

      expect(user).toBeTruthy()
      expect(user?.id).toBeTruthy()
      expect(user?.name).toBe('any_name')
      expect(user?.email).toBe('any_email@mail.com')
      expect(user?.password).toBe('any_password')
      expect(user?.userType.id).toBeTruthy()
      expect(user?.userType.name).toBe('any_name')
      expect(user?.userType.role).toBe('any_role')
    })

    test('Should return null if not found a user type', async () => {
      const sut = makeSut()

      const user = await sut.add({
        name: 'any_name',
        email: 'any_email@mail.com',
        password: 'any_password',
        userTypeRole: 'any_role'
      })

      expect(user).toBeNull()
    })
  })

  describe('loadByEmail()', () => {
    test('Should return an user on loadByEmail success', async () => {
      const sut = makeSut()

      await userCollection.insertOne({
        name: 'any_name',
        email: 'any_email@mail.com',
        password: 'any_password'
      })

      const user = await sut.loadByEmail('any_email@mail.com')

      expect(user).toBeTruthy()
      expect(user?.id).toBeTruthy()
      expect(user?.name).toBe('any_name')
      expect(user?.email).toBe('any_email@mail.com')
      expect(user?.password).toBe('any_password')
    })

    test('Should return null if loadByEmail fails', async () => {
      const sut = makeSut()

      const user = await sut.loadByEmail('any_email@mail.com')

      expect(user).toBeFalsy()
    })
  })

  describe('updateAccessToken()', () => {
    test('Should update the user accessToken on updateAccessToken success', async () => {
      const sut = makeSut()

      const res = await userCollection.insertOne({
        name: 'any_name',
        email: 'any_email@mail.com',
        password: 'any_password'
      })

      const fakeUser = res.ops[0]

      expect(fakeUser.accessToken).toBeFalsy()

      await sut.updateAccessToken(fakeUser._id, 'any_token')

      const user = await userCollection.findOne({ _id: fakeUser._id })

      expect(user).toBeTruthy()
      expect(user?.accessToken).toBe('any_token')
    })
  })

  describe('loadByToken()', () => {
    test('Should return an user on loadByToken without role', async () => {
      const sut = makeSut()

      await userCollection.insertOne({
        name: 'any_name',
        email: 'any_email@mail.com',
        password: 'any_password',
        accessToken: 'any_token'
      })

      const user = await sut.loadByToken('any_token')

      expect(user).toBeTruthy()
      expect(user?.id).toBeTruthy()
      expect(user?.name).toBe('any_name')
      expect(user?.email).toBe('any_email@mail.com')
      expect(user?.password).toBe('any_password')
    })

    test('Should return an user on loadByToken with admin role', async () => {
      const sut = makeSut()

      const resType = await userTypeCollection.findOne({ role: 'admin' })

      await userCollection.insertOne({
        name: 'any_name',
        email: 'any_email@mail.com',
        password: 'any_password',
        accessToken: 'any_token',
        userTypeId: resType._id
      })

      const user = await sut.loadByToken('any_token', 'admin')

      expect(user).toBeTruthy()
      expect(user?.id).toBeTruthy()
      expect(user?.name).toBe('any_name')
      expect(user?.email).toBe('any_email@mail.com')
      expect(user?.password).toBe('any_password')
    })

    test('Should return null on loadByToken with invalid role', async () => {
      const sut = makeSut()

      await userTypeCollection.insertOne({
        name: 'Dirigente',
        role: 'dirigente'
      })

      const resType = await userTypeCollection.findOne({ role: 'dirigente' })

      await userCollection.insertOne({
        name: 'any_name',
        email: 'any_email@mail.com',
        password: 'any_password',
        accessToken: 'any_token',
        userTypeId: resType._id
      })

      const user = await sut.loadByToken('any_token', 'admin')

      expect(user).toBeFalsy()
    })

    test('Should return an user on loadByToken if user is admin', async () => {
      const sut = makeSut()

      await userCollection.insertOne({
        name: 'any_name',
        email: 'any_email@mail.com',
        password: 'any_password',
        accessToken: 'any_token',
        role: 'admin'
      })

      const user = await sut.loadByToken('any_token')

      expect(user).toBeTruthy()
      expect(user?.id).toBeTruthy()
      expect(user?.name).toBe('any_name')
      expect(user?.email).toBe('any_email@mail.com')
      expect(user?.password).toBe('any_password')
    })

    test('Should return null if loadByToken fails', async () => {
      const sut = makeSut()

      const user = await sut.loadByToken('any_token')

      expect(user).toBeFalsy()
    })
  })

  describe('loadAll()', () => {
    test('Should load all users on success', async () => {
      const userType = await userTypeCollection.insertOne({
        name: 'Administrador',
        role: 'admin'
      })

      await userCollection.insertMany([{
        name: 'any_name',
        email: 'any_email@mail.com',
        password: 'hashed_password',
        userTypeId: userType.ops[0]._id
      }, {
        name: 'other_name',
        email: 'other_email@mail.com',
        password: 'hashed_password',
        userTypeId: userType.ops[0]._id
      }])

      const sut = makeSut()

      const users = await sut.loadAll()
      expect(users.length).toBe(2)
      expect(users[0].name).toBe('any_name')
      expect(users[1].name).toBe('other_name')
    })

    test('Should empty list', async () => {
      const sut = makeSut()

      const users = await sut.loadAll()
      expect(users.length).toBe(0)
    })
  })
})
