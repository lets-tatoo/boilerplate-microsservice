import { MongoHelper } from '../helpers/mongo-helper'
import { AddUserRepository } from '@/data/protocols/db/user/add-user-repository'
import { LoadUserByEmailRepository } from '@/data/protocols/db/user/load-user-by-email-repository'
import { LoadUserByTokenRepository } from '@/data/protocols/db/user/load-user-by-token-repository'
import { LoadUsersRepository } from '@/data/protocols/db/user/load-users-repository'
import { UpdateAccessTokenRepository } from '@/data/protocols/db/user/update-access-token-repository'
import { UserModel } from '@/domain/models/user'
import { AddUserModel } from '@/domain/usecases/add-user'

export class UserMongoRepository implements
  AddUserRepository,
  LoadUserByEmailRepository,
  UpdateAccessTokenRepository,
  LoadUserByTokenRepository,
  LoadUsersRepository {
  async add (userData: AddUserModel): Promise<UserModel | null> {
    const userCollection = await MongoHelper.getCollection('users')
    const userTypeCollection = await MongoHelper.getCollection('user-types')

    const userType = await userTypeCollection.findOne({ role: userData.userTypeRole })

    if (!userType) {
      return null
    }

    const { name, email, password, firstAccess } = userData

    const result = await userCollection.insertOne({
      name,
      email,
      password,
      userTypeId: userType._id,
      fisrtAccess: firstAccess ?? false
    })

    const newUser = Object.assign({}, result.ops[0], { userTypeId: undefined, userType: MongoHelper.map(userType) })

    return MongoHelper.map(newUser)
  }

  async loadByEmail (email: string): Promise<UserModel | null> {
    const userCollection = await MongoHelper.getCollection('users')

    const user = await userCollection.findOne({ email })

    return user && MongoHelper.map(user)
  }

  async updateAccessToken (id: string, token: string): Promise<void> {
    const userCollection = await MongoHelper.getCollection('users')

    await userCollection.updateOne({ _id: id }, { $set: { accessToken: token } })
  }

  async loadByToken (token: string, role?: string | undefined): Promise<UserModel | null> {
    const userCollection = await MongoHelper.getCollection('users')
    const userTypeCollection = await MongoHelper.getCollection('user-types')
    let objFindOne = {}

    if (role) {
      const orArray: Array<{ userTypeId: string }> = []
      const userTypeIdAmin = await userTypeCollection.findOne({ role: 'admin' })
      orArray.push({
        userTypeId: userTypeIdAmin._id
      })

      if (role !== 'admin') {
        const userType = await userTypeCollection.findOne({ role })

        if (userType) {
          orArray.push({
            userTypeId: userType._id
          })
        }
      }

      objFindOne = {
        accessToken: token,
        $or: orArray
      }
    } else {
      objFindOne = {
        accessToken: token
      }
    }

    const user = await userCollection.findOne(objFindOne)

    return user && MongoHelper.map(user)
  }

  async loadAll (): Promise<UserModel[]> {
    const userCollection = await MongoHelper.getCollection('users')

    const users: UserModel[] = await userCollection.find().toArray()

    return users
  }
}
