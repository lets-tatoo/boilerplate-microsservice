import { MongoHelper as sut } from './mongo-helper'

describe('Mongo Helper', () => {
  beforeAll(async () => {
    const url = process.env.MONGO_URL as string
    await sut.connect(url)
  })

  afterAll(async () => {
    await sut.disconnect()
  })

  test('Should reconnect  if mongodb is down', async () => {
    let userCollection = await sut.getCollection('users')
    expect(userCollection).toBeTruthy()
    await sut.disconnect()
    userCollection = await sut.getCollection('users')
    expect(userCollection).toBeTruthy()
  })
})
