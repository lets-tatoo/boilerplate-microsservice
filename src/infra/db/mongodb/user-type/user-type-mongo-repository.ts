import { MongoHelper } from '../helpers/mongo-helper'
import { AddUserTypeRepository, AddUserTypeModel } from '@/data/usecases/add-user-type/db-add-user-type-protocols'

export class UserTypeMongoRepository implements AddUserTypeRepository {
  async add (userTypeData: AddUserTypeModel): Promise<void> {
    const userTypeCollection = await MongoHelper.getCollection('user-types')

    await userTypeCollection.insertOne(userTypeData)
  }
}
