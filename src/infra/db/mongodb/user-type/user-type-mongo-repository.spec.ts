import { UserTypeMongoRepository } from './user-type-mongo-repository'
import { MongoHelper } from '../helpers/mongo-helper'
import { Collection } from 'mongodb'

describe('User Mongo Repository', () => {
  const url = process.env.MONGO_URL as string
  let userTypeCollection: Collection

  beforeAll(async () => {
    await MongoHelper.connect(url)
  })

  afterAll(async () => {
    await MongoHelper.disconnect()
  })

  beforeEach(async () => {
    userTypeCollection = await MongoHelper.getCollection('user-types')
    await userTypeCollection.deleteMany({})
  })

  const makeSut = (): UserTypeMongoRepository => {
    return new UserTypeMongoRepository()
  }

  test('Should add an user-type on add success', async () => {
    const sut = makeSut()

    await sut.add({
      name: 'any_name',
      role: 'any_role'
    })

    const userType = await userTypeCollection.findOne({ name: 'any_name' })

    expect(userType).toBeTruthy()
  })
})
