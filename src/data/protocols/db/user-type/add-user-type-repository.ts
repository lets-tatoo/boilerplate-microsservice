import { AddUserTypeModel } from '@/domain/usecases/add-user-type'

export interface AddUserTypeRepository {
  add: (user: AddUserTypeModel) => Promise<void>
}
