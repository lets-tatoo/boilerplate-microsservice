import { UserModel } from '@/data/usecases/add-user/db-add-user-protocols'

export interface LoadUsersRepository {
  loadAll: () => Promise<UserModel[]>
}
