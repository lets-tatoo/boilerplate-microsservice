import {
  Authentication,
  AuthenticationModel,
  HashComparer,
  Encrypter,
  LoadUserByEmailRepository,
  UpdateAccessTokenRepository
} from './db-authentication-protocols'

export class DbAuthentication implements Authentication {
  constructor (
    private readonly loadUserByEmailRepository: LoadUserByEmailRepository,
    private readonly hashComparer: HashComparer,
    private readonly encrypter: Encrypter,
    private readonly updateAccessTokenRepository: UpdateAccessTokenRepository
  ) {}

  async auth (authentication: AuthenticationModel): Promise<string | null> {
    const user = await this.loadUserByEmailRepository.loadByEmail(authentication.email)

    if (user) {
      const isValid = await this.hashComparer.comparer(authentication.password, user.password)

      if (isValid) {
        const accessToken = await this.encrypter.encrypt(user.id)

        await this.updateAccessTokenRepository.updateAccessToken(user.id, accessToken)

        return accessToken
      }
    }

    return null
  }
}
