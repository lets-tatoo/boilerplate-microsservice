import { UserModel } from '../add-user/db-add-user-protocols'
import { LoadUsersRepository } from '@/data/protocols/db/user/load-users-repository'
import { LoadUsers } from '@/domain/usecases/load-users'

export class DbLoadUsers implements LoadUsers {
  constructor (private readonly loadUsersRepository: LoadUsersRepository) {}

  async load (): Promise<UserModel[]> {
    const users = await this.loadUsersRepository.loadAll()

    return users
  }
}
