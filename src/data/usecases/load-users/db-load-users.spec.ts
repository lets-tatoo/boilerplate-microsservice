import { DbLoadUsers } from './db-load-users'
import { LoadUsersRepository } from '@/data/protocols/db/user/load-users-repository'
import { UserModel } from '@/domain/models/user'

const makeFakeUsers = (): UserModel[] => ([{
  id: 'valid_id',
  name: 'valid_name',
  email: 'valid_email@mail.com',
  password: 'hashed_password',
  userType: {
    id: 'valid_id',
    name: 'valid_name',
    role: 'valid_role'
  }
}])

type SutTypes = {
  sut: DbLoadUsers
  loadUsersRepositoryStub: LoadUsersRepository
}

const makeLoadUsersRepository = (): LoadUsersRepository => {
  class LoadUsersRepositoryStub implements LoadUsersRepository {
    async loadAll (): Promise<UserModel[]> {
      return await new Promise(resolve => resolve(makeFakeUsers()))
    }
  }

  return new LoadUsersRepositoryStub()
}

const makeSut = (): SutTypes => {
  const loadUsersRepositoryStub = makeLoadUsersRepository()
  const sut = new DbLoadUsers(loadUsersRepositoryStub)

  return { sut, loadUsersRepositoryStub }
}

describe('DbLoadUsers', () => {
  test('Should call LoadUsersRepository', async () => {
    const { sut, loadUsersRepositoryStub } = makeSut()

    const loadAllSpy = jest.spyOn(loadUsersRepositoryStub, 'loadAll')

    await sut.load()
    expect(loadAllSpy).toHaveBeenCalled()
  })

  test('Should return a list of Users on success', async () => {
    const { sut } = makeSut()

    const users = await sut.load()
    expect(users).toEqual(makeFakeUsers())
  })

  test('Should throw if LoadUsersRepository throws', async () => {
    const { sut, loadUsersRepositoryStub } = makeSut()

    jest.spyOn(loadUsersRepositoryStub, 'loadAll').mockReturnValueOnce(new Promise((_resolve, reject) => reject(new Error())))

    const promise = sut.load()
    await expect(promise).rejects.toThrow()
  })
})
