import {
  Randomize
} from './db-randomize-protocols'

export class DbRandomize implements Randomize {
  constructor (private readonly size: number) {}

  generate (): string {
    return Math.random().toString(this.size)
  }
}
