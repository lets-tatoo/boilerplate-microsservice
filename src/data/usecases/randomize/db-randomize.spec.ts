import { DbRandomize } from './db-randomize'

type SutTypes = {
  sut: DbRandomize
}

const makeSut = (): SutTypes => {
  const sut = new DbRandomize(36)

  return { sut }
}

describe('DbAuthentication UseCase', () => {
  test('Should return a string if success', async () => {
    const { sut } = makeSut()

    const string = sut.generate()

    expect(string).toBeTruthy()
  })
})
