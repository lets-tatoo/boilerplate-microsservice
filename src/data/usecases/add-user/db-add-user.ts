import { LoadUserByEmailRepository } from '../authentication/db-authentication-protocols'
import { AddUser, AddUserModel, Hasher, AddUserRepository, AddUSerResponseModel } from './db-add-user-protocols'

export class DbAddUser implements AddUser {
  constructor (
    private readonly hasher: Hasher,
    private readonly addUserRepository: AddUserRepository,
    private readonly loadUserByEmailRepository: LoadUserByEmailRepository
  ) {}

  async add (userData: AddUserModel): Promise<AddUSerResponseModel> {
    const user = await this.loadUserByEmailRepository.loadByEmail(userData.email)

    if (user) {
      return { error: 'EmailInUseError' }
    }

    const hashedPassword = await this.hasher.hash(userData.password)

    const newUser = await this.addUserRepository.add(Object.assign({}, userData, { password: hashedPassword }))

    if (!newUser) {
      return { error: 'UserTypeNotFoundError' }
    }

    return { data: newUser }
  }
}
