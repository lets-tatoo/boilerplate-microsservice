import { LoadUserByEmailRepository } from '../authentication/db-authentication-protocols'
import { DbAddUser } from './db-add-user'
import { Hasher, AddUserModel, UserModel, AddUserRepository } from './db-add-user-protocols'

type SutTypes = {
  sut: DbAddUser
  hasherStub: Hasher
  addUserRepositoryStub: AddUserRepository
  loadUserByEmailRepositoryStub: LoadUserByEmailRepository
}

const makeAddUserRepository = (): AddUserRepository => {
  class AddUserRepositoryStub implements AddUserRepository {
    async add (user: AddUserModel): Promise<UserModel> {
      return await new Promise(resolve => resolve(makeFakeUser()))
    }
  }

  return new AddUserRepositoryStub()
}

const makeHasher = (): Hasher => {
  class HasherStub implements Hasher {
    async hash (value: string): Promise<string> {
      return await new Promise(resolve => resolve('hashed_password'))
    }
  }

  return new HasherStub()
}

const makeLoadUserByEmailRepository = (): LoadUserByEmailRepository => {
  class LoadUserByEmailRepositoryStub implements LoadUserByEmailRepository {
    async loadByEmail (email: string): Promise<UserModel | null> {
      return await new Promise(resolve => resolve(null))
    }
  }

  return new LoadUserByEmailRepositoryStub()
}

const makeSut = (): SutTypes => {
  const hasherStub = makeHasher()
  const addUserRepositoryStub = makeAddUserRepository()
  const loadUserByEmailRepositoryStub = makeLoadUserByEmailRepository()
  const sut = new DbAddUser(hasherStub, addUserRepositoryStub, loadUserByEmailRepositoryStub)

  return { sut, hasherStub, addUserRepositoryStub, loadUserByEmailRepositoryStub }
}

const makeFakeUser = (): UserModel => ({
  id: 'valid_id',
  name: 'valid_name',
  email: 'valid_email@mail.com',
  password: 'hashed_password',
  userType: {
    id: 'valid_id',
    name: 'valid_name',
    role: 'valid_role'
  }
})

const makeFakeUserData = (): AddUserModel => ({
  name: 'valid_name',
  email: 'valid_email@mail.com',
  password: 'valid_password',
  userTypeRole: 'valid_role'
})

describe('DbAddUser Usecase', () => {
  test('Should call Hasher with correct password', async () => {
    const { sut, hasherStub } = makeSut()

    const hashSpy = jest.spyOn(hasherStub, 'hash')

    await sut.add(makeFakeUserData())
    expect(hashSpy).toHaveBeenCalledWith('valid_password')
  })

  test('Should throw if Hasher throws', async () => {
    const { sut, hasherStub } = makeSut()

    jest.spyOn(hasherStub, 'hash').mockReturnValueOnce(new Promise((_resolve, reject) => reject(new Error())))

    const promise = sut.add(makeFakeUserData())
    await expect(promise).rejects.toThrow()
  })

  test('Should call AddUserRepository with correct values', async () => {
    const { sut, addUserRepositoryStub } = makeSut()

    const addSpy = jest.spyOn(addUserRepositoryStub, 'add')

    await sut.add(makeFakeUserData())
    expect(addSpy).toHaveBeenCalledWith({
      name: 'valid_name',
      email: 'valid_email@mail.com',
      password: 'hashed_password',
      userTypeRole: 'valid_role'
    })
  })

  test('Should throw if AddUserRepository throws', async () => {
    const { sut, addUserRepositoryStub } = makeSut()

    jest.spyOn(addUserRepositoryStub, 'add').mockReturnValueOnce(new Promise((_resolve, reject) => reject(new Error())))

    const promise = sut.add(makeFakeUserData())
    await expect(promise).rejects.toThrow()
  })

  test('Should return a error if AddUserRepository returns null', async () => {
    const { sut, addUserRepositoryStub } = makeSut()

    jest.spyOn(addUserRepositoryStub, 'add').mockReturnValueOnce(new Promise(resolve => resolve(null)))

    const user = await sut.add(makeFakeUserData())
    expect(user.data).toBeFalsy()
    expect(user.error).toBe('UserTypeNotFoundError')
  })

  test('Should return an user on success', async () => {
    const { sut } = makeSut()

    const user = await sut.add(makeFakeUserData())
    expect(user).toEqual({ data: makeFakeUser() })
  })

  test('Should call LoadUserByEmailRepository with correct email', async () => {
    const { sut, loadUserByEmailRepositoryStub } = makeSut()

    const loadSpy = jest.spyOn(loadUserByEmailRepositoryStub, 'loadByEmail')

    await sut.add(makeFakeUserData())

    expect(loadSpy).toHaveBeenCalledWith('valid_email@mail.com')
  })

  test('Should return a error if LoadUserByEmailRepository not return null', async () => {
    const { sut, loadUserByEmailRepositoryStub } = makeSut()

    jest.spyOn(loadUserByEmailRepositoryStub, 'loadByEmail').mockReturnValueOnce(new Promise(resolve => resolve(makeFakeUser())))

    const user = await sut.add(makeFakeUserData())
    expect(user.data).toBeFalsy()
    expect(user.error).toBe('EmailInUseError')
  })
})
