import { DbAddUserType } from './db-add-user-type'
import { AddUserTypeModel, AddUserTypeRepository } from './db-add-user-type-protocols'

const makeFakeUserTypeData = (): AddUserTypeModel => ({
  name: 'any_name',
  role: 'any_role'
})

const makeAddUserTypeRepository = (): AddUserTypeRepository => {
  class AddUserTypeRepositoryStub implements AddUserTypeRepository {
    async add (user: AddUserTypeModel): Promise<void> {}
  }

  return new AddUserTypeRepositoryStub()
}

type SutTypes = {
  sut: DbAddUserType
  addUserTypeRepositoryStub: AddUserTypeRepository
}

const makeSut = (): SutTypes => {
  const addUserTypeRepositoryStub = makeAddUserTypeRepository()

  const sut = new DbAddUserType(addUserTypeRepositoryStub)

  return { sut, addUserTypeRepositoryStub }
}

describe('DbAddUserType Usecase', () => {
  test('Should call AddUserTypeRepository with correct values', async () => {
    const { sut, addUserTypeRepositoryStub } = makeSut()

    const addSpy = jest.spyOn(addUserTypeRepositoryStub, 'add')

    const userTypeData = makeFakeUserTypeData()

    await sut.add(userTypeData)

    expect(addSpy).toHaveBeenCalledWith(userTypeData)
  })

  test('Should throws if AddUserTypeRepository throws', async () => {
    const { sut, addUserTypeRepositoryStub } = makeSut()

    jest.spyOn(addUserTypeRepositoryStub, 'add').mockReturnValueOnce(new Promise((_resolve, reject) => reject(new Error())))

    const userTypeData = makeFakeUserTypeData()

    const promise = sut.add(userTypeData)

    await expect(promise).rejects.toThrow()
  })
})
