import { AddUserType, AddUserTypeModel, AddUserTypeRepository } from './db-add-user-type-protocols'

export class DbAddUserType implements AddUserType {
  constructor (private readonly addUserTypeRepository: AddUserTypeRepository) {}

  async add (data: AddUserTypeModel): Promise<void> {
    await this.addUserTypeRepository.add(data)
  }
}
