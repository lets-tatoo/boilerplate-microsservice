import { UserModel } from '../add-user/db-add-user-protocols'
import { Decrypter } from '@/data/protocols/criptography/decrypter'
import { LoadUserByTokenRepository } from '@/data/protocols/db/user/load-user-by-token-repository'
import { LoadUserByToken } from '@/domain/usecases/load-user-by-token'

export class DbLoadUserByToken implements LoadUserByToken {
  constructor (private readonly decrypter: Decrypter, private readonly loadUserByTokenRepository: LoadUserByTokenRepository) {}

  async load (accessToken: string, role?: string): Promise<UserModel | null> {
    const token = await this.decrypter.decrypt(accessToken)

    if (token) {
      const user = await this.loadUserByTokenRepository.loadByToken(accessToken, role)

      if (user) {
        return user
      }
    }

    return null
  }
}
