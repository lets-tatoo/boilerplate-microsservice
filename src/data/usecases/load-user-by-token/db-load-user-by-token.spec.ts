import { DbLoadUserByToken } from './db-load-user-by-token'
import { UserModel } from '../add-user/db-add-user-protocols'
import { Decrypter } from '@/data/protocols/criptography/decrypter'
import { LoadUserByTokenRepository } from '@/data/protocols/db/user/load-user-by-token-repository'

type SutTypes = {
  sut: DbLoadUserByToken
  decrypterStub: Decrypter
  loadUserByTokenRepositoryStub: LoadUserByTokenRepository
}

const makeFakeUser = (): UserModel => ({
  id: 'valid_id',
  name: 'valid_id',
  email: 'valid_email@mail.com',
  password: 'hashed_password',
  userType: {
    id: 'valid_id',
    name: 'valid_name',
    role: 'valid_role'
  }
})

const makeLoadUserByTokenRepository = (): LoadUserByTokenRepository => {
  class LoadUserByTokenRepositoryStub implements LoadUserByTokenRepository {
    async loadByToken (token: string, role?: string): Promise<UserModel | null> {
      return await new Promise(resolve => resolve(makeFakeUser()))
    }
  }

  return new LoadUserByTokenRepositoryStub()
}

const makeDecrypter = (): Decrypter => {
  class DecrypterStub implements Decrypter {
    async decrypt (value: string): Promise<string | null> {
      return await new Promise(resolve => resolve('any_value'))
    }
  }

  return new DecrypterStub()
}

const makeSut = (): SutTypes => {
  const decrypterStub = makeDecrypter()
  const loadUserByTokenRepositoryStub = makeLoadUserByTokenRepository()
  const sut = new DbLoadUserByToken(decrypterStub, loadUserByTokenRepositoryStub)

  return { sut, decrypterStub, loadUserByTokenRepositoryStub }
}

describe('DbLoadUserByToken UseCase', () => {
  test('Should call Decrypter with correct values', async () => {
    const { sut, decrypterStub } = makeSut()

    const decryptSpy = jest.spyOn(decrypterStub, 'decrypt')

    await sut.load('any_token', 'any_role')
    expect(decryptSpy).toHaveBeenCalledWith('any_token')
  })

  test('Should return null if Decrypter retuns null', async () => {
    const { sut, decrypterStub } = makeSut()

    jest.spyOn(decrypterStub, 'decrypt').mockReturnValueOnce(new Promise(resolve => resolve(null)))

    const user = await sut.load('any_token', 'any_role')
    expect(user).toBeNull()
  })

  test('Should call LoadUserByTokenRepository with correct values', async () => {
    const { sut, loadUserByTokenRepositoryStub } = makeSut()

    const loadByTokenSpy = jest.spyOn(loadUserByTokenRepositoryStub, 'loadByToken')

    await sut.load('any_token', 'any_role')
    expect(loadByTokenSpy).toHaveBeenCalledWith('any_token', 'any_role')
  })

  test('Should return null if LoadUserByTokenRepository retuns null', async () => {
    const { sut, loadUserByTokenRepositoryStub } = makeSut()

    jest.spyOn(loadUserByTokenRepositoryStub, 'loadByToken').mockReturnValueOnce(new Promise(resolve => resolve(null)))

    const user = await sut.load('any_token', 'any_role')
    expect(user).toBeNull()
  })

  test('Should return an user on success', async () => {
    const { sut } = makeSut()

    const user = await sut.load('any_token', 'any_role')
    expect(user).toEqual(makeFakeUser())
  })

  test('Should throw if Decrypter throws', async () => {
    const { sut, decrypterStub } = makeSut()

    jest.spyOn(decrypterStub, 'decrypt').mockReturnValueOnce(new Promise((_resolve, reject) => reject(new Error())))

    const promise = sut.load('any_token', 'any_role')
    await expect(promise).rejects.toThrow()
  })

  test('Should throw if LoadUserByTokenRepository throws', async () => {
    const { sut, loadUserByTokenRepositoryStub } = makeSut()

    jest.spyOn(loadUserByTokenRepositoryStub, 'loadByToken').mockReturnValueOnce(new Promise((_resolve, reject) => reject(new Error())))

    const promise = sut.load('any_token', 'any_role')
    await expect(promise).rejects.toThrow()
  })
})
