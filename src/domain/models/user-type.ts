export type UserTypeModel = {
  id: string
  name: string
  role: string
}
