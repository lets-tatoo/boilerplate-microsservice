import { UserTypeModel } from './user-type'

export type UserModel = {
  id: string
  name: string
  email: string
  password: string
  userType: UserTypeModel
  fisrtAccess?: boolean
}
