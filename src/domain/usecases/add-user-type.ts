import { UserTypeModel } from '../models/user-type'

export type AddUserTypeModel = Omit<UserTypeModel, 'id'>

export interface AddUserType {
  add: (data: AddUserTypeModel) => Promise<void>
}
