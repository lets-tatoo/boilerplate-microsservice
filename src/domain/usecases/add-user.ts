import { UserModel } from '@/domain/models/user'

export type AddUserModel = {
  name: string
  email: string
  password: string
  userTypeRole: string
  firstAccess?: boolean
}

export type AddUSerResponseModel = {
  data?: UserModel
  error?: string
}

export interface AddUser {
  add: (user: AddUserModel) => Promise<AddUSerResponseModel>
}
