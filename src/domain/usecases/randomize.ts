export interface Randomize {
  generate: () => string
}
