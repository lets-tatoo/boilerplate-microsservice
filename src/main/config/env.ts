export default {
  mongoUrl: process.env.MONGO_URL ?? 'mongodb+srv://letstatoo:root@letstatoo.53hbd.mongodb.net/boilerplate-microsservice?retryWrites=true&w=majority',
  port: process.env.PORT ?? 5050,
  jwtSecret: process.env.JWT_SECRET ?? 'boilerplate-microsservice'
}
