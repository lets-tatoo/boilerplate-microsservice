import { makeAddUserTypeValidation } from './add-user-type-validation-factory'
import { RequiredFieldValidation, ValidationComposite } from '@/validation/validators'
import { Validation } from '@/presentation/protocols/validation'

jest.mock('@/validation/validators/validation-composite')

describe('UserTypeValidation Factory', () => {
  test('Should call ValidationComposite with all validations', () => {
    makeAddUserTypeValidation()

    const validations: Validation[] = []

    for (const field of ['name', 'role']) {
      validations.push(new RequiredFieldValidation(field))
    }

    expect(ValidationComposite).toHaveBeenCalledWith(validations)
  })
})
