import { makeAddUserTypeValidation } from './add-user-type-validation-factory'
import { makeLogControllerDecorator } from '@/main/factories/decorators/log-controller-decorator-factory'
import { makeDbAddUserType } from '@/main/factories/usecases/user-type/add-user-type/db-user-type-factory'
import { AddUserTypeController } from '@/presentation/controllers/user-type/add-user-type/add-user-type-controller'
import { Controller } from '@/presentation/protocols'

export const makeAddUserTypeController = (): Controller => {
  const addUserTypeController = new AddUserTypeController(makeAddUserTypeValidation(), makeDbAddUserType())

  return makeLogControllerDecorator(addUserTypeController)
}
