import { RequiredFieldValidation, ValidationComposite } from '@/validation/validators'
import { Validation } from '@/presentation/protocols/validation'

export const makeAddUserTypeValidation = (): ValidationComposite => {
  const validations: Validation[] = []

  for (const field of ['name', 'role']) {
    validations.push(new RequiredFieldValidation(field))
  }

  return new ValidationComposite(validations)
}
