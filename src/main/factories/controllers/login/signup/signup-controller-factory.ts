import { makeSignUpValidation } from './signup-validation-factory'
import { makeLogControllerDecorator } from '@/main/factories/decorators/log-controller-decorator-factory'
import { makeDbAddUser } from '@/main/factories/usecases/user/add-user/db-add-user-factory'
import { makeDbAuthentication } from '@/main/factories/usecases/user/authentication/db-authentication-factory'
import { SignUpController } from '@/presentation/controllers/login/signup/signup-controller'
import { Controller } from '@/presentation/protocols'

export const makeSignUpController = (): Controller => {
  const signUpController = new SignUpController(makeDbAddUser(), makeSignUpValidation(), makeDbAuthentication())

  return makeLogControllerDecorator(signUpController)
}
