import { makeLogControllerDecorator } from '@/main/factories/decorators/log-controller-decorator-factory'
import { makeDbLoadUsers } from '@/main/factories/usecases/user/load-users/db-load-users-factory'
import { LoadUsersController } from '@/presentation/controllers/user/load-users/load-users-controller'
import { Controller } from '@/presentation/protocols'

export const makeLoadUsersController = (): Controller => {
  const addUserTypeController = new LoadUsersController(makeDbLoadUsers())

  return makeLogControllerDecorator(addUserTypeController)
}
