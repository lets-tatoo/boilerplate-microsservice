import { makeDbLoadUserByToken } from '../usecases/user/load-user-by-token/db-load-user-by-token-factory'
import { AuthMiddleware } from '@/presentation/middlewares/auth-middleware'
import { Middleware } from '@/presentation/protocols'

export const makeAuthMiddleware = (role?: string): Middleware => {
  return new AuthMiddleware(makeDbLoadUserByToken(), role)
}
