import env from '@/main/config/env'
import { DbLoadUserByToken } from '@/data/usecases/load-user-by-token/db-load-user-by-token'
import { LoadUserByToken } from '@/domain/usecases/load-user-by-token'
import { UserMongoRepository } from '@/infra/db/mongodb/user/user-mongo-repository'
import { JwtAdapter } from '@/infra/criptography/jwt-adapter/jwt-adapter'

export const makeDbLoadUserByToken = (): LoadUserByToken => {
  const jwtAdapter = new JwtAdapter(env.jwtSecret)
  const userRepository = new UserMongoRepository()

  return new DbLoadUserByToken(jwtAdapter, userRepository)
}
