import { DbLoadUsers } from '@/data/usecases/load-users/db-load-users'
import { LoadUsers } from '@/domain/usecases/load-users'
import { UserMongoRepository } from '@/infra/db/mongodb/user/user-mongo-repository'

export const makeDbLoadUsers = (): LoadUsers => {
  const userRepository = new UserMongoRepository()

  return new DbLoadUsers(userRepository)
}
