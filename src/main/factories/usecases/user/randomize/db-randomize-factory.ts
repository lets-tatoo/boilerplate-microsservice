import { Randomize } from '@/domain/usecases/randomize'
import { DbRandomize } from '@/data/usecases/randomize/db-randomize'

export const makeDbRandomize = (): Randomize => {
  const size = 36

  return new DbRandomize(size)
}
