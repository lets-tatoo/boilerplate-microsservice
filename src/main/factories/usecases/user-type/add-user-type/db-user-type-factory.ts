import { DbAddUserType } from '@/data/usecases/add-user-type/db-add-user-type'
import { AddUserType } from '@/domain/usecases/add-user-type'
import { UserTypeMongoRepository } from '@/infra/db/mongodb/user-type/user-type-mongo-repository'

export const makeDbAddUserType = (): AddUserType => {
  const userTypeRepository = new UserTypeMongoRepository()

  return new DbAddUserType(userTypeRepository)
}
