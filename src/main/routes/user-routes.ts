import { adaptRoute } from '../adapters/express-routes-adapter'
import { makeLoadUsersController } from '../factories/controllers/user/load-users/load-users-controller-factory'
import { makeAddUserController } from '../factories/controllers/user/add-user/add-user-controller.factory'
import { adminAuth } from '../middlewares/admin-auth'
import { Router } from 'express'

export default (router: Router): void => {
  router.get('/users', adminAuth, adaptRoute(makeLoadUsersController()))
  router.post('/users', adminAuth, adaptRoute(makeAddUserController()))
}
