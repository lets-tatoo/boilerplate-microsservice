import app from '../config/app'
import env from '../config/env'
import { MongoHelper } from '@/infra/db/mongodb/helpers/mongo-helper'
import { Collection } from 'mongodb'
import request from 'supertest'
import { sign } from 'jsonwebtoken'

describe('UserType Routes', () => {
  const url = process.env.MONGO_URL as string
  let userTypeCollection: Collection
  let userCollection: Collection

  beforeAll(async () => {
    await MongoHelper.connect(url)
  })

  afterAll(async () => {
    await MongoHelper.disconnect()
  })

  beforeEach(async () => {
    userTypeCollection = await MongoHelper.getCollection('user-types')
    userCollection = await MongoHelper.getCollection('users')

    await userTypeCollection.insertOne({
      name: 'Administrator',
      role: 'admin'
    })
  })

  afterEach(async () => {
    await userTypeCollection.deleteMany({})
    await userCollection.deleteMany({})
  })

  describe('POST /user-types', () => {
    test('Should return 403 on user-type without accessToken', async () => {
      await request(app)
        .post('/api/user-types')
        .send({
          name: 'Dirigente',
          role: 'dirigente'
        })
        .expect(403)
    })

    test('Should return 204 on user-type with valid accessToken', async () => {
      const resType = await userTypeCollection.findOne({ role: 'admin' })

      const res = await userCollection.insertOne({
        name: 'Lucas Limeres',
        email: 'lucas.limeres@gmail.com',
        password: '123',
        userTypeId: resType._id
      })

      const id = res.ops[0]._id
      const accessToken = sign({ id }, env.jwtSecret)
      await userCollection.updateOne({ _id: id }, {
        $set: {
          accessToken
        }
      })

      await request(app)
        .post('/api/user-types')
        .set('x-access-token', accessToken)
        .send({
          name: 'Dirigente',
          role: 'dirigente'
        })
        .expect(204)
    })
  })
})
