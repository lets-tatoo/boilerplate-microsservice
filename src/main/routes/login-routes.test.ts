import app from '../config/app'
import { MongoHelper } from '@/infra/db/mongodb/helpers/mongo-helper'
import { Collection } from 'mongodb'
import request from 'supertest'
import { hash } from 'bcrypt'

describe('Login Routes', () => {
  const url = process.env.MONGO_URL as string
  let userCollection: Collection
  let userTypeCollection: Collection

  beforeAll(async () => {
    await MongoHelper.connect(url)
  })

  afterAll(async () => {
    await MongoHelper.disconnect()
  })

  beforeEach(async () => {
    userCollection = await MongoHelper.getCollection('users')
    await userCollection.deleteMany({})
    userTypeCollection = await MongoHelper.getCollection('user-types')
    await userTypeCollection.deleteMany({})
  })

  describe('POST /signup', () => {
    test('Should return 200 on signup', async () => {
      await userTypeCollection.insertOne({
        name: 'Aluno',
        role: 'aluno'
      })

      await request(app)
        .post('/api/signup')
        .send({
          name: 'Lucas Limeres',
          email: 'lucas.limeres@gmail.com',
          userTypeRole: 'aluno',
          password: '123',
          passwordConfirmation: '123'
        })
        .expect(200)
    })
  })

  describe('POST /login', () => {
    test('Should return 200 on login', async () => {
      await userCollection.insertOne({
        name: 'Lucas Limeres',
        email: 'lucas.limeres@gmail.com',
        password: await hash('123', 12)
      })

      await request(app)
        .post('/api/login')
        .send({
          email: 'lucas.limeres@gmail.com',
          password: '123'
        })
        .expect(200)
    })

    test('Should return 401 on login', async () => {
      await request(app)
        .post('/api/login')
        .send({
          email: 'lucas.limeres@gmail.com',
          password: '123'
        })
        .expect(401)
    })
  })
})
