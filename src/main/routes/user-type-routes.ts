import { adaptRoute } from '../adapters/express-routes-adapter'
import { makeAddUserTypeController } from '../factories/controllers/user-type/add-user-type/add-user-type-controller-factory'
import { adminAuth } from '../middlewares/admin-auth'
import { Router } from 'express'

export default (router: Router): void => {
  router.post('/user-types', adminAuth, adaptRoute(makeAddUserTypeController()))
}
