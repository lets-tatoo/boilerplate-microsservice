import app from '../config/app'
import env from '../config/env'
import { MongoHelper } from '@/infra/db/mongodb/helpers/mongo-helper'
import { Collection } from 'mongodb'
import request from 'supertest'
import { sign } from 'jsonwebtoken'

let userTypeCollection: Collection
let userCollection: Collection

const makeAccessToken = async (): Promise<string> => {
  const resType = await userTypeCollection.findOne({ role: 'admin' })

  const res = await userCollection.insertOne({
    name: 'Lucas Limeres',
    email: 'lucas.limeres@gmail.com',
    password: '123',
    userTypeId: resType._id
  })

  const id = res.ops[0]._id
  const accessToken = sign({ id }, env.jwtSecret)

  await userCollection.updateOne({ _id: id }, {
    $set: {
      accessToken
    }
  })

  return accessToken
}

describe('User Routes', () => {
  const url = process.env.MONGO_URL as string

  beforeAll(async () => {
    await MongoHelper.connect(url)
  })

  afterAll(async () => {
    await MongoHelper.disconnect()
  })

  beforeEach(async () => {
    userTypeCollection = await MongoHelper.getCollection('user-types')
    userCollection = await MongoHelper.getCollection('users')

    await userTypeCollection.insertOne({
      name: 'Administrator',
      role: 'admin'
    })
  })

  afterEach(async () => {
    await userTypeCollection.deleteMany({})
    await userCollection.deleteMany({})
  })

  describe('GET /users', () => {
    test('Should return 403 on user-type without accessToken', async () => {
      await request(app)
        .get('/api/users')
        .expect(403)
    })

    test('Should return 200 on load users with valid accessToken', async () => {
      const accessToken = await makeAccessToken()

      await request(app)
        .get('/api/users')
        .set('x-access-token', accessToken)
        .expect(200)
    })
  })
})
